import json
from pathlib import Path
import secrets

from flask import Flask, abort, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    base_dir = Path(__file__).absolute().parent

    with base_dir.joinpath('settings.json').open('r') as f:
        settings = json.loads(f.read())

    header_token = request.headers.get('X-Gitlab-Token', '')
    if not secrets.compare_digest(header_token, settings['gitlab_token']):
        abort(403)

    target_directory = Path(settings['target_directory'])

    event = request.headers.get('X-Gitlab-Event')
    if event is None:
        abort(400)

    payload = request.get_json()

    if event == 'Push Hook':
        kind = payload.get('object_kind')  # Should always be 'push'
        name = payload.get('project', {}).get('path_with_namespace', '').split('/')[-1]

        # Push events provide a full Git ref in 'ref', such as "refs/heads/master"
        branch = payload['ref'].split('/', 2)[2]

        # Hopefully somebody is monitoring one of these
        target_directory.joinpath(f"{kind}-{name}-{branch}").touch()
        target_directory.joinpath(f"{kind}-{name}").touch()

    return 'ok'

if __name__ == '__main__':
    app.run()
